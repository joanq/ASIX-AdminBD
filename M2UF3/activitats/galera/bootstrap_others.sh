#!/bin/sh

# Instal·lació
apt-get update
apt-get install -y apt-transport-https curl
curl -o /etc/apt/trusted.gpg.d/mariadb_release_signing_key.asc 'https://mariadb.org/mariadb_release_signing_key.asc'
sh -c "echo 'deb https://mirror.mva-n.net/mariadb/repo/10.9/debian bullseye main' >>/etc/apt/sources.list"
apt-get update
apt-get install -y mariadb-server
