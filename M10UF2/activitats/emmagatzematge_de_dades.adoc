= Activitat emmagatzematge de dades
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Article 1.

SAN i NAS es fan servir per emmagatzemar dades en una xarxa. La diferència
bàsica entre ells són els protocols que es fan servir, però a nivell empresarial
a vegades hem de prendre decisions que van més enllà de les tecnologies.

Llegeix
link:https://www.2ksystems.com/blog/14-blog-sistemas-servidores/123-almacenamiento-diferencia-nas-san[aquest article]
de l'empresa 2kSystem i contesta les preguntes que tens a continuació.

1. Què vol dir que NAS treballa a nivell fitxer i SAN ho fa a nivell de bloc?

2. Quin sistema recomana l’article per emmagatzemar bases de dades i per què?

3. En quins casos recomana SAN i en quins casos NAS?

4. Busca preus de mercat actuals, tant de SAN com de NAS.

== Article 2.

A partir de l’article
link:https://searchdatacenter.techtarget.com/es/cronica/Principales-tendencias-de-tecnologia-de-almacenamiento-de-datos-para-2017[principals tendències de tecnologia d'emmagatzematge de dades pel 2017],
contesta les preguntes següents:

1. Quins avantatges ofereix tenir les còpies de seguretat al núvol? Quins
desavantatges?

2. A l’article diu: “en mayo (del 2016), una interrupción de Salesforce impidió
a los clientes acceder a sus datos durante varias horas”. Busca més informació
sobre aquest fet i explica breument què va passar i a quines empreses va
afectar.

3. Explica en què consisteix Docker i com afecta a l’emmagatzematge de dades.

4. “Samsung presentó una unidad de estado sólido SAS de 2,5 pulgadas y 15 TB
(capacidad real de 15,36 TB) en 2015”. Busca informació d’aquest any per veure
quines són aquestes xifres en les unitats Flash actualment.

5. En què consisteix la tecnologia NVMe?

6. Busca qui és Avinash Lakshman i perquè parlen d’ell a l’article.
