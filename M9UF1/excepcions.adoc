= Activitat: excepcions
:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:
:icons: font

Per a cada exercici crea un fitxer dins de la carpeta `html/activitat2` amb nom
`exercici1.php`, `exercici2.php`, etc.

La codificació de la pàgina ha de ser UTF-8, la pàgina ha de validar amb HTML 5
i l'aspecte del codi html que es genera ha de ser fàcilment llegible,
estar ben identat, etc.

[NOTE]
====
Utilitza excepcions per separar les situacions d'error de l'execució exitosa
dels programes.
====

1. Crea una web que presenti un formulari en què es demani a l'usuari introduir
dos nombres. El formulari enviarà aquests dos nombres per POST.
+
Cal comprovar que els valors enviats són efectivament dos nombres. Si algun dels
dos no ho és es mostrarà el missatge adequat i es retornarà al formulari
original.
+
En cas que tots dos siguin nombres, es mostrarà per pantalla tots els nombres
enters compresos entre ells (inclosos ells mateixos) i un enllaç per tornar al
formulari.
+
Si el primer nombre és menor que el segon l'ordre dels nombres generats serà
creixent i en cas contrari, decreixent.
+
Per exemple si passem el 5 i el 10 s'ha d'imprimir 5,6,7,8,9,10. Si passem el
12 i el 7 s'ha d'imprimir 12,11,10,9,8,7.
+
Els nombres generats s'han de mostrar en horitzontal i separats la mida d'un
tabulador.

2. Volem crear un formulari per crear reserves en el nostre hotel. Les dades
que demanarem són:
+
--
- Nom i cognoms del client. Són camps obligatoris d'almenys dos caràcters
cadascun.
- Correu electrònic. En aquesta versió comprovarem que hi hagi una arrova, que
hi hagi almenys un caràcter abans de l'arrova, i que hi hagi un punt seguit de
dos o tres caràcters més al final de la cadena.
- Nacionalitat. Obligatori, almenys dos caràcters.
- Número de telèfon. Opcional. Només pot contenir nombres, excepte el primer
caràcter que pot ser un +. Ha de tenir entre 9 i 13 caràcters.
- La data d'entrada i la data de sortida. Són dates, han d'estar en el futur, i
la data d'entrada ha de ser anterior a la data de sortida.
- Número d'hostes. Ha de ser un nombre entre 1 i 5.
--
+
Fes que es puguin introduir aquestes dades i que el sistema informi de qualsevol
error que es produeixi.
+
Si no hi ha cap error, mostrarem les dades de la reserva i permetrem tornar al
principi.
