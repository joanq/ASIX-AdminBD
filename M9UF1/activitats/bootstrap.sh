#!/bin/sh

# Instal·lació MariaDB
apt-get update
apt-get install -y mariadb-server

# Instal·lació Apache i PHP
apt-get install -y apache2 libapache2-mod-php php-mysql
cp /vagrant/main-directory.conf /etc/apache2/conf-available
a2enconf main-directory
cp /vagrant/main-site.conf /etc/apache2/sites-available
a2dissite 000-default
a2ensite main-site
systemctl reload apache2

# Configuració PHP
sed -i s/"^display_errors .*"/"display_errors = On"/ /etc/php/8.2/apache2/php.ini
sed -i s/"^display_startup_errors .*"/"display_startup_errors = On"/ /etc/php/8.2/apache2/php.ini
sed -i s/"^error_reporting .*"/"error_reporting = E_ALL"/ /etc/php/8.2/apache2/php.ini
sed -i s/"^track_errors .*"/"track_errors = On"/ /etc/php/8.2/apache2/php.ini

# Configuració de MariaDB:
# - Permet connexions des de qualsevol host
# - Activa GROUP BY estricte
# - Permet || com a CONCAT (PIPES_AS_CONCAT)
# - No permet " com a delimitador de cadenes, només ' (ANSI_QUOTES)
cp /vagrant/50-server.cnf /etc/mysql/mariadb.conf.d

# Creació base de dades
mysql << EOF
SOURCE /vagrant/hotel.sql
CREATE OR REPLACE USER webuser@'localhost' IDENTIFIED BY 'super3';
GRANT ALL ON hotel.* TO webuser@'localhost';
CREATE OR REPLACE USER admin@'%' IDENTIFIED BY 'super3';
GRANT ALL ON hotel.* TO admin@'%';
EOF
systemctl restart mariadb
