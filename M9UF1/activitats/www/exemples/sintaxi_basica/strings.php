<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Strings</title>
  </head>
  <body>
    <main role="main" class="container">
      <h1 class="mt-5">Exemples de cadenes de text</h1>
      <h2 class="mt-5">Qualsevol cadena</h2>
      <?php
      echo "<h3>Concatenar cadenes</h3>";
      $text1="abc";
      $text2="def";
      echo "<p>Si concatenem $text1 i $text2 queda ".$text1.$text2.".</p>";
      echo "<h3>Eliminar espai en blanc als extrems d'una cadena</h3>";
      $text="  tinc dos espais al davant i al darrera  ";
      echo "<p>La cadena '$text' després d'eliminar-hi espais als extrems queda '".trim($text)."'.</p>";
      $text="Hola món";
      echo "<h3>Substitució d'un text per un altre</h3>";
      echo "<p>A '$text', subsituïm 'Hola' per 'Adéu': '".str_replace('Hola', 'Adéu', $text)."'</p>";

      ?>
      <h2 class="mt-5">Cadenes sense accents o caràcters no anglesos</h2>
      <?php
      $text="Hola mon";
      echo "<h3>Longitud</h3>";
      echo "<p>La cadena '$text' té ".strlen($text)." caràcters/bytes.</p>";
      echo "<h3>Accés a caràcters individuals</h3>";
      echo "<p>El tercer caràcter/byte de '$text' és {$text[2]}.</p>";
      echo "<h3>Recorregut per tots els caràcters</h3>";
      for ($n=0; $n<strlen($text); $n++) {
        echo "<p>Posició $n, caràcter/byte {$text[$n]}</p>";
      }
      echo "<h3>Cerca d'un text dins d'un altre text</h3>";
      echo "<p>La cadena 'la' apareix dins de $text a la posició ".strpos($text,'la').".</p>";
      echo "<p>Apareix la cadena 'q' dins de $text? ".strpos($text,'q')."</p>";
      echo "<h3>Passar una cadena a majúsucles/minúscules</h3>";
      echo "<p>".strtoupper($text)."</p>";
      echo "<p>".strtolower($text)."</p>";
      echo "<h3>Obtenir part d'una cadena</h3>";
      echo "<p>A partir del segon agafem 5 caràcters de '$text': ".substr($text, 1, 5)."</p>";
      ?>
      <h2 class="mt-5">Cadenes UTF8</h2>
      <?php
      $text="Hola món";
      echo "<h3>Longitud</h3>";
      echo "<p>La cadena '$text' té ".mb_strlen($text)." caràcters.</p>";
      echo "<h3>Accés a caràcters individuals</h3>";
      echo "<p>El tercer caràcter de '$text' és ".mb_substr($text, 2, 1);
      echo "<h3>Recorregut per tots els caràcters</h3>";
      for ($n=0; $n<mb_strlen($text); $n++) {
        echo "<p>Posició $n, caràcter ".mb_substr($text, $n, 1)."</p>";
      }
      echo "<h3>Cerca d'un text dins d'un altre text</h3>";
      echo "<p>La cadena 'ón' apareix dins de $text a la posició ".mb_strpos($text,'ón').".</p>";
      echo "<h3>Passar una cadena a majúsucles/minúscules</h3>";
      echo "<p>".mb_strtoupper($text)."</p>";
      echo "<p>".mb_strtolower($text)."</p>";
      echo "<h3>Obtenir part d'una cadena</h3>";
      echo "<p>A partir del segon agafem 6 caràcters de '$text': ".mb_substr($text, 1, 6)."</p>";
      ?>
      <h2>Funcions per cadenes</h2>
      <a href="https://secure.php.net/manual/en/ref.strings.php">Funcions per cadenes</a><br>
      <a href="https://www.php.net/manual/en/book.mbstring.php">Funcions per cadenes multibyte</a><br>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
