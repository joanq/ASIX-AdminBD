<?php
function connect() {
  require_once 'dbconfig.php';

  try {
    $conn = new PDO("mysql:host={$dbconfig['server']};dbname={$dbconfig['db']};charset=utf8",
        $dbconfig['username'], $dbconfig['password']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $conn;
  } catch (PDOException $e) {
    $_SESSION['error']='Ha fallat la connexió: '.$e->getMessage();
    header('Location: error.php');
    exit();
  }
}

?>
