<?php
require_once 'Connection.php';

function check_room($conn) {
  try {
    if (!isset($_POST['roomNumber'])) {
      throw new Exception("Falten paràmetres.");
    }
    $roomNumber = trim($_POST['roomNumber']);
    $st = $conn->prepare("SELECT RoomNumber FROM Rooms WHERE RoomNumber=:roomNumber");
    $st->bindParam(':roomNumber', $roomNumber);
    $st->execute();
    if (sizeof($st->fetchAll())==0) {
      throw new Exception("No existeix aquest número d'habitació.");
    }
    return $roomNumber;
  } catch (Exception $e) {
    $_SESSION['error'] = $e->getMessage();
    header('Location: index.php');
    exit();
  }
}

function parseDate($date) {
  $parsedDate = date_parse($date);
  if ($parsedDate['error_count']>0) {
    throw new Exception("La data no té un format vàlid.");
  }
  $strDate = "{$parsedDate['year']}-{$parsedDate['month']}-{$parsedDate['day']}";
  return $strDate;
}

function check_date($conn) {
  try {
    if (!isset($_POST['date'])) {
      throw new Exception("Falten paràmetres.");
    }
    $date = trim($_POST['date']);
    $parsedDate = parseDate($date);
    return $parsedDate;
  } catch (Exception $e) {
    $_SESSION['error'] = $e->getMessage();
    header('Location: index.php');
    exit();
  }
}

function get_hosts($conn, $roomNumber, $date) {
  try {
    $statement = $conn->prepare(
      "SELECT h.Id, FirstName, LastName, DocType, DocNumber, Nationality
      FROM Hosts h
      JOIN StayHosts sh ON h.Id=sh.HostId
      JOIN Stays s ON s.Id=sh.StayId
      WHERE RoomNumber=:roomNumber
      AND (CheckOut>:date1 OR CheckOut IS NULL)
      AND CheckIn<=:date2"
    );
    $statement->bindParam(':roomNumber', $roomNumber);
    $statement->bindParam(':date1', $date);
    $statement->bindParam(':date2', $date);
    $statement->execute();
    $hosts = $statement->fetchAll();
    return $hosts;
  } catch(PDOException $e) {
    $_SESSION['error'] = "No s'ha pogut recuperar la llista d'hostes:\n{$e->getMessage()}\n";
    header('Location: index.php');
    exit();
  }
}

function show_hosts($hosts) {
  if (sizeof($hosts)>0) {
    echo "<table class='table table-striped'>\n<tr><th>Id</th><th>Nom</th><th>Cognom</th><th>Tipus de document</th><th>Número de document</th><th>Nacionalitat</th></tr>\n";
    foreach ($hosts as $host) {
      echo "<tr><td>{$host['Id']}</td><td>{$host['FirstName']}</td><td>{$host['LastName']}</td><td>{$host['DocType']}</td><td>{$host['DocNumber']}</td><td>{$host['Nationality']}</td></tr>\n";
    }
    echo "</table>\n";
  } else {
    echo "<p>No hi ha hostes en aquesta habitació per aquesta data.</p>\n";
  }
}

session_start();
$conn = connect();
$roomNumber = check_room($conn);
$date = check_date($conn);
$hosts = get_hosts($conn, $roomNumber, $date);

?>
<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Sentències SELECT</title>
  </head>
  <body>
    <main role="main" class="container">
      <h1 class="mt-5">Hostes</h1>
      <?php show_hosts($hosts); ?>
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
